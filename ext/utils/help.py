from discord.ext.commands import DefaultHelpCommand
from discord import Forbidden


class PMHelpCommand(DefaultHelpCommand):
    """Reimplementing pm_help=None in a class."""

    async def send_pages(self):
        user = self.context.author
        pages = list(self.paginator.pages)

        # for a single page, just return it
        if len(pages) == 1:
            await self.context.send(pages[0])
            return

        # for multiple pages, always dm
        for page in self.paginator.pages:
            try:
                await user.send(page)
            except Forbidden:
                await self.context.send("failed to DM help.")
